package http

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// Client ...
type Client struct {
	http.Client
}

// Do2JSON ...
func (c *Client) Do2JSON(req *http.Request, header http.Header, jsonResponse interface{}) (*http.Response, error) {
	req.Header = header
	respHTTP, err := c.Do(req)
	if err != nil {
		return nil, err
	}

	respBody, err := ioutil.ReadAll(respHTTP.Body)
	if err != nil {
		return nil, err
	}
	return respHTTP, json.Unmarshal(respBody, &jsonResponse)
}

// Get2JSON ...
func (c *Client) Get2JSON(url string, header http.Header, params map[string]string, jsonResponse interface{}) (*http.Response, error) {
	if header == nil {
		header = http.Header{}
	}
	header.Set("Accept", "application/json")
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	if len(params) != 0 {
		q := req.URL.Query()
		for key, value := range params {
			q.Add(key, value)
		}
		req.URL.RawQuery = q.Encode()
	}
	return c.Do2JSON(req, header, &jsonResponse)
}

// PostJSON2JSON ...
func (c *Client) PostJSON2JSON(url string, header http.Header, body interface{}, jsonResponse interface{}) (*http.Response, error) {
	if header == nil {
		header = http.Header{}
	}
	header.Set("Content-Type", "application/json")
	header.Set("Accept", "application/json")
	bodyByte, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", url, bytes.NewReader(bodyByte))
	if err != nil {
		return nil, err
	}
	return c.Do2JSON(req, header, &jsonResponse)
}
