package http_test

import (
	"testing"

	bhttp "bitbucket.org/baslenvm/bgo/http"
)

func TestGet2JSON(t *testing.T) {
	type GetResp struct {
		UserID    int    `json:"userId"`
		ID        int    `json:"id"`
		Title     string `json:"title"`
		Completed bool   `json:"completed"`
	}
	resp := GetResp{}
	client := &bhttp.Client{}
	client.Get2JSON("https://jsonplaceholder.typicode.com/todos/1", nil, nil, &resp)
	t.Logf("%+v\n", resp)
	if resp.UserID != 1 || resp.ID != 1 || resp.Title != "delectus aut autem" || resp.Completed != false {
		t.Error("Get2JSON")
	}
}

func TestPostJSON2JSON(t *testing.T) {
	type DataReq struct {
		UserID int    `json:"userId"`
		Title  string `json:"title"`
		Body   string `json:"body"`
	}
	type DataResp struct {
		UserID int    `json:"userId"`
		ID     int    `json:"id"`
		Title  string `json:"title"`
		Body   string `json:"body"`
	}
	req := DataReq{1, "fdfgdgdg aut dfsoooo", "i fix"}
	resp := DataResp{}
	client := &bhttp.Client{}
	client.PostJSON2JSON("https://jsonplaceholder.typicode.com/posts", nil, req, &resp)
	t.Logf("%+v\n", req)
	t.Logf("%+v\n", resp)
	if resp.UserID != 1 || resp.ID != 101 || resp.Title != "fdfgdgdg aut dfsoooo" || resp.Body != "i fix" {
		t.Error("PostJSON2JSON")
	}
}
